import mitt from 'mitt';
import {th} from "vuetify/lib/locale";

export const emitter = mitt()
let wrapper = document.createElement('dev');
let images = undefined;
export default class Gif {
    static get toolbox() {
        return {
            title: 'Gif',
            icon: '<svg width="17" height="15" viewBox="0 0 336 276" xmlns="http://www.w3.org/2000/svg"><path d="M291 150V79c0-19-15-34-34-34H79c-19 0-34 15-34 34v42l67-44 81 72 56-29 42 30zm0 52l-43-30-56 30-81-67-66 39v23c0 19 15 34 34 34h178c17 0 31-13 34-29zM79 0h178c44 0 79 35 79 79v118c0 44-35 79-79 79H79c-44 0-79-35-79-79V79C0 35 35 0 79 0z"/></svg>'
        };
    }

    constructor({data, api, config}) {
        this.data = data;
        this.config = config || {};
    }
    render() {
        if (images === undefined) {
            this.config.uploadImage().then((selectedImages) => {
                images = selectedImages
                selectedImages.forEach(item => {
                    let dev = document.createElement('dev');
                    dev.classList.add('col')
                    let image = document.createElement('img');
                    image.style.maxWidth = '240px'
                    image.style.height = '240px'
                    image.style.padding = '10px'
                    dev.appendChild(image)
                    image.src = item.src
                    wrapper.appendChild(dev)
                })

                this.render()
            })
        }
        images = undefined
        wrapper = document.createElement('dev');
        return wrapper
    }
    save(blockContent) {
        const images = blockContent.querySelectorAll('img');
        let data = []
        images.forEach(image => {
            data.push(image.src)
        })
        return {
            images: data
        }
    }
}
