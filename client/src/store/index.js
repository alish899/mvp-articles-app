import { createStore } from 'vuex'
import API from '../../controllers/content';
import createPersistedState from 'vuex-persistedstate'
import router from "@/router";
const getDefaultState = () => ({
  token: null,
  loggingIn: false,
  loginError: null,
  user: {
    id: null,
    username: null,
    firstName: null,
    lastName: null
  },
})
const store =  createStore({
  strict: true,
  plugins: [createPersistedState()],
  state: getDefaultState(),
  getters: {
    isLoggedIn: state => state.token,
    getUser: state => state.user,
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USER: (state, user) => {
      state.user = user
    },
    RESET: state => {
      Object.assign(state, getDefaultState())
    },
    LOGIN_START: state => {
      state.loggingIn = true
    },
    LOGIN_COMPLETE: (state, errorMessage) => {
      state.loggingIn = false
      state.loginError = errorMessage
    },
  },
  actions: {
    login({ commit }, loginData) {
      commit('LOGIN_START')
      API.login(...loginData)
          .then(response => {
            const token = response.content.accessToken
            commit('SET_TOKEN', token)
            commit('SET_USER', response.content)
            commit('LOGIN_COMPLETE', null)
            router.push('/')
          })
          .catch(error => {
            console.log(error)
            commit('LOGIN_COMPLETE', error)
          })
    },
    getUser({ commit }) {
      API.getUser().then(response => {
        commit('SET_USER', response.content)
      }).catch(error => {
        console.log(error)
      })
    },
    logout: ({ commit }) => {
      API.logout()
      commit('RESET', '')
    },
  },
  modules: {
  }
})
export default store