import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'HomeView',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/add-article',
    name: 'AddContentView',
    component: () => import('../views/AddContentView.vue')
  },
  {
    path: '/content/:id',
    name: 'ContentView',
    component: () => import('../views/ContentView.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
