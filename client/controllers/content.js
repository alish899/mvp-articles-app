import * as Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from "@/store";
const app = Vue.createApp({})
app.use(VueAxios, axios)

const url = 'http://localhost:5000/api'  // node js server
const ApiUrl = 'http://localhost:8000/api/' // laravel server

export default class API {
    static login(credentials) {
        return axios.post(`${ApiUrl}client/auth/login`, credentials)
            .then(response => response.data)
    }
    static logout() {
        axios.defaults.headers.common.Authorization = `Bearer ${store.getters.isLoggedIn}`
         axios
            .post(`${ApiUrl}client/auth/logout`)
            .then(response => response.data)
    }

    static getUser() {
        return API.get(`${url}client/auth/getUser`).then(response => response.data)
    }
    static async findGifs(key) {
        const res = await axios.get(url + '/gifs/findGifs/' + key);
        return res.data;
    }
    static async createArticle(data) {
        axios.defaults.headers.common.Authorization = `Bearer ${store.getters.isLoggedIn}`
        const res = await axios.post(ApiUrl + 'client/articles', data);
        return res.data;
    }
    static async getAllArticles() {
        axios.defaults.headers.common.Authorization = `Bearer ${store.getters.isLoggedIn}`
        const res = await axios.get(ApiUrl + 'client/articles');
        return res.data.content;
    }
    static async getArticleById(id) {
        axios.defaults.headers.common.Authorization = `Bearer ${store.getters.isLoggedIn}`
        const res = await axios.get(ApiUrl + 'client/articles/' + id);
        return res.data.content;
    }
}
