
# 🌟 MVP Article App 

create articles using editor js and Gif api


## 💡 Features

- login
- logout
- Create Article for logged in user
- view articles for all user


## ⚙️ Installation

Install from package.json
```bash
  cd server
  npm install
  
  cd client
  npm install
```

Run Server
```bash
  npm run dev
```

Run Client
```bash
  npm run serve
```
    
## ❓ FAQ

#### Article model

- title : string
- content : Gifs object

Login data:
user: ali
password: 123456

#### using tools
- NodeJS
- VueJS


## 🧍 Authors

- [Ali shaaban]

