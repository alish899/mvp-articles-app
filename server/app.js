// PORT & URL MongoDB
require('dotenv/config')
const port = process.env.PORT;

// Express
const express = require('express');

// Cors
const cors = require('cors');

// Routes
const routes = require('./routes/routes');

// Middlewares
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("uploads"));
app.use("/api", routes);
// Server
app.listen(port, () => {
    console.log(`Server start on http://localhost:${process.env.PORT}`);
});
