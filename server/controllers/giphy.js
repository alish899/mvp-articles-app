const fs = require('fs');
const http = require('http');
module.exports = class Giphy_API {

    // Fetch All Posts
    static async findGif(req, res) {
        try {
            let queryString = req.params.key;
            // ENCODE THE QUERY STRING TO REMOVE WHITE SPACES AND RESTRICTED CHARACTERS
            let term = encodeURIComponent(queryString);
            // PUT THE SEARCH TERM INTO THE GIPHY API SEARCH URL
            let url = 'http://api.giphy.com/v1/gifs/search?q=' + term + '&api_key=tsG3kjK4A84NN6kEH1tzbLmrBP86loxU';
            http.get(url, (response) => {
                // SET ENCODING OF RESPONSE TO UTF8
                response.setEncoding('utf8');
                let body = '';
                // listens for the event of the data buffer and stream
                response.on('data', (d) => {
                    // CONTINUOUSLY UPDATE STREAM WITH DATA FROM GIPHY
                    body += d;
                });
                // once it gets data it parses it into json
                response.on('end', () => {
                    // WHEN DATA IS FULLY RECEIVED PARSE INTO JSON
                    let parsed = JSON.parse(body);
                    // RENDER THE HOME TEMPLATE AND PASS THE GIF DATA IN TO THE TEMPLATE
                    // res.render('search-giphy', { gifs: parsed.data })
                    res.status(200).json(parsed.data);
                });
            });

        } catch (err) {
            res.status(500).json(err);
        }
    }
}