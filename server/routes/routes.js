const express = require('express');
const router = express.Router();
const giphy_API = require('../controllers/giphy');

// Content API
router.get('/gifs/findGifs/:key', giphy_API.findGif);

module.exports = router;